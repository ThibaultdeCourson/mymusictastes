# Basic packages
import warnings
from os.path import isfile

import pandas as pd
import json
from io import StringIO

# Graphics
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from dash import Dash, html, dash_table, dcc, callback, Output, Input, State
import dash_bootstrap_components as dbc

# Local functions
from common import connect_to_Spotify, sequence_queries
from retrieve_data import retrieve_data, identify_non_original_tracks

print("\nBUILDING WEBAPP")

df_columns = {'artists': ['artist_name', 'followers', 'genre_group_id', 'popularity', 'artist_uri'],
              'teams': ['team_id', 'artist_id'],
              'tracks': ['track_name', 'details', 'album_id', 'team_id', 'duration', 'explicit', 'track_uri'],
              'playlists': ['playlist_name', 'description', 'public'],
              'tracks_playlists': ['playlist_id', 'track_id', 'date'],
              'albums': ['album_name', 'album_team_id', 'album_type', 'album_size', 'genre_group_id', 'popularity',
                         'release_date', 'album_uri'],
              'groups_genres': ['genre_group_id', 'genre_id'],
              'genres': ['name']}

# Initialize the app - incorporate css
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP],
           prevent_initial_callbacks=True, suppress_callback_exceptions=True)

app.layout = dbc.Container([
    # dcc.Store(id='store'),
    dbc.Row([
        html.H1("Spotify data analysis"),
        html.Hr(),
        html.Button("Refresh data", id='refresh_data', n_clicks=0, style={'width': 'auto'}),
        html.Hr(),

        dbc.Tabs([
            dbc.Tab(label="Manage data", tab_id='manage'),
            dbc.Tab(label="Playlist analysis", tab_id='playlists'),
            dbc.Tab(label="Explore data", tab_id='explore')],
            id='tabs',
            active_tab='playlists'),
    ]),
    dbc.Row(id='tab_content', className='p-4', align='start'),
    #dcc.Loading(id='load_tab_content', type='circle', children=),
    # dcc.Store stores intermediate values
    dcc.Store(id='data')
],
    fluid=True
)


# Generates the tabs
# @app.callback(Output('load_tab_content', 'children'),
#               Input('tabs', 'active_tab'),
#               Input('data', 'data'))
@app.callback(
    Output('tab_content', 'children'),
    Input('tabs', 'active_tab'),
    Input('data', 'data')
    # Input('store', 'data')
)
def render_tab_content(active_tab, data):
    """
    This callback takes the 'active_tab' property as input, as well as the
    stored graphs, and renders the tab content depending on what the value of
    'active_tab' is.
    """
    datasets = json.loads(data)
    playlists = pd.read_json(StringIO(datasets['raw_data']['playlists']), orient='split')
    to_process = pd.read_json(StringIO(datasets['to_process']), orient='split')

    if not to_process.empty:
        to_process.drop(columns=['track_uri'], inplace=True)

    if active_tab:  # and data is not None:
        if active_tab == 'manage':
            print("\n[MANAGE DATA] TAB SELECTED")
            return [
                dbc.Card([
                    html.H3("Import data from Spotify's API"),
                    dbc.Row([
                        dbc.Col("Reset local data:", width='auto'),
                        dbc.Col(dcc.Checklist(['tracks', 'albums', 'artists', 'genres', 'playlists'],
                                              inline=True, inputStyle={'margin-left': '15px', 'margin-right': '7px'},
                                              id='df_to_reset'),
                                width='auto'),
                        dbc.Col([
                            html.Button("Import data", id='import_data', n_clicks=0),
                            html.Div(id='import_data_button', children='')
                        ], width='auto')]
                    )
                ], style={'padding': 20, 'width': 'auto', 'margin': 30}),
                html.Br(),
                html.H3("Original tracks"),
                html.Button("Generate the 'original tracks' playlist", id='original_tracks', n_clicks=0,
                            style={'width': 'auto'}),
                html.Br(),
                dcc.Loading(id='load_original_tracks', type='circle',
                            children=html.Div(id='original_tracks_output', children='')),
                html.Br(),
                html.H3("Tracks to categorize or like"),
                html.Button("Generate the 'to categorize' playlist", id='to_categorize', n_clicks=0,
                            style={'width': 'auto'}),
                html.Br(),
                dcc.Loading(id='load_to_categorize_output', type='circle',
                            children=html.Div(id='to_categorize_output', children='')),
                html.Br(),
                dcc.Loading(id='load_to_categorize_table', type='circle',
                            children=html.Div(dash_table.DataTable(to_process.to_dict('records'),
                                                                   id='to_categorize_table', sort_action='native')))
            ]
        elif active_tab == 'playlists':
            print("\n[PLAYLIST ANALYSIS] TAB SELECTED")
            return [
                html.Br(),
                dbc.Row([
                    dbc.Col([
                        dbc.Row([
                            dbc.Col([
                                "Group choice",
                                dcc.Dropdown([] if playlists.empty else playlists.group.unique().tolist(),
                                             None, id='group_choice')
                            ]),
                            dbc.Col([
                                "Playlist choice",
                                dcc.Dropdown(options={}, value=None, id='playlist_choice')
                            ])
                        ]),
                        dcc.Loading(id='load_playlist_treemap', type='circle',
                                    children=html.Div(dcc.Graph(figure={}, id='playlist_treemap',
                                                                style={'height': '80vh', 'width': '100%'}))
                                    ),
                    ], md=5),
                    dbc.Col([
                        html.H3('The music genres of the playlists selected'),
                        "Minimal frequency of the genres to display:",
                        dcc.Input(id='threshold', value=1, type='number', step=0.1, min=-1,
                                  max=100),
                        # round(100*genre_hierarchy.sort_values(by='count').iloc[-2, 1]/n_total_genres, 1)
                        "%",
                        dcc.Loading(id='load_genre_graph', type='circle',
                                    children=html.Div(dcc.Graph(figure={}, id='genre_graph',
                                                                style={'height': '80vh', 'width': '100%'}))
                        ),
                    ], md=6)
                ], justify='around'),
                dbc.Row(
                    dbc.Col([
                        html.H3('Tracks in the selected playlists'),
                        dcc.Loading(id='load_playlist_table', type='circle',
                                    children=html.Div(dash_table.DataTable(id='playlist_table', sort_action='native',
                                                                           page_size=25),
                                                      style={'maxHeight': '1000px', 'overflow': 'scroll'})
                        )
                    ])
                )
            ]
        elif active_tab == 'explore':
            print("\n[EXPLORE DATA] TAB SELECTED")
            return [
                dbc.Col([
                    html.H2('Compare playlists'),
                    html.Label('Comparison criteria'),
                    dcc.RadioItems(options=['release_date', 'duration', 'popularity'], value='release_date',
                                   id='stat-chosen', inputStyle={'margin-left': '15px', 'margin-right': '7px'},
                                   inline=True),
                    html.Br(),
                    dcc.Loading(id='load_box_playlists_graph', type='circle', children=html.Div(
                        dcc.Graph(figure={}, id='playlists-graph', style={'height': str(35 * len(playlists)) + 'px'})
                    ))
                ])
            ]
    return "No tab selected"


# Refresh button: generates datasets
@callback(
    Output('data', 'data'),
    Input('refresh_data', 'n_clicks'))
def clean_data(value):
    print("\nFETCHING LOCAL DATA")

    data = dict()
    (teams_full, tracks_full, tracks_in_playlists, not_categorized_tracks, not_liked_tracks, genre_hierarchy,
     to_process, playlist_groups) = [pd.DataFrame()] * 8
    for variable in ('artists', 'teams', 'tracks', 'playlists', 'tracks_playlists', 'albums', 'groups_genres',
                     'genres'):
        filename = 'data/' + variable + '.pkl'
        if isfile(filename):
            data[variable] = pd.read_pickle(filename)
        else:
            data[variable] = pd.DataFrame(columns=df_columns[variable])
    print("Local data fetched")

    if not data['artists'].empty and not data['albums'].empty:
        print(round(100 * data['artists'].genre_group_id.count() / len(data['artists'])),
              "% of artists with genre data, ",
              round(100 * data['albums'].genre_group_id.count() / len(data['albums'])), "% of albums with genre data",
              sep='')

    # List of team names
    if not data['teams'].empty and not data['artists'].empty:
        teams_full = (
            data['teams'].merge(data['artists'], left_on='artist_id', right_index=True)
            .groupby('team_id')
            .agg(n_artist=pd.NamedAgg(column='artist_name', aggfunc='count'),
                 artist_names=pd.NamedAgg(column='artist_name', aggfunc=', '.join))
        )

    if not data['tracks'].empty and not data['tracks_playlists'].empty and not data['playlists'].empty:
        tracks_full = (
            data['tracks'].merge(data['tracks_playlists'], left_index=True, right_on='track_id', how='left')
            .merge(data['playlists'], left_on='playlist_id', right_index=True, how='left')
            .rename(columns={'date': 'date_added', 'description': 'playlist_description', 'public': 'public_playlist'})
            .set_index('track_id')
            .drop(columns=['playlist_id', 'date_added', 'playlist_description', 'public_playlist', 'n_tracks', 'genre'])
            .merge(
                data['tracks'].merge(data['tracks_playlists'][data['tracks_playlists'].playlist_id != 0],
                                     left_index=True, right_on='track_id', how='left')
                .merge(data['playlists'], left_on='playlist_id', right_index=True, how='left')
                .groupby(['track_id'])
                .agg(playlists=pd.NamedAgg(column='playlist_name', aggfunc=lambda var: ', '.join(var[var.notnull()]))),
                left_index=True, right_index=True
            )
        )

    if not data['tracks'].empty and not data['tracks_playlists'].empty and not data[
        'playlists'].empty and not teams_full.empty:
        tracks_in_playlists = (
            data['tracks'].merge(data['tracks_playlists'][data['tracks_playlists'].playlist_id.isin(
                data['playlists'][data['playlists'].genre].index)],
                                 left_index=True,
                                 right_on='track_id', how='left')
            .merge(data['playlists'], left_on='playlist_id', right_index=True, how='left')
            .groupby('track_id')
            .agg(occurrences=pd.NamedAgg(column='playlist_name', aggfunc='count'),
                 playlists=pd.NamedAgg(column='playlist_name', aggfunc=lambda var: ', '.join(var[var.notnull()])))
            .merge(data['tracks'], left_index=True, right_index=True)
            .merge(teams_full, left_on='team_id', right_index=True)
            .drop(['album_id', 'duration', 'explicit'], axis=1)
            .merge(  # Adds the list of the playlists not associated to a genre
                data['tracks'].merge(data['tracks_playlists'][~data['tracks_playlists'].playlist_id.isin(
                    data['playlists'][data['playlists'].genre].index)
                                                              & (data['tracks_playlists'].playlist_id != 0)],
                                     left_index=True,
                                     right_on='track_id')
                .merge(data['playlists'], left_on='playlist_id', right_index=True, how='left')
                .groupby('track_id')
                .agg(other_playlists=pd.NamedAgg(column='playlist_name', aggfunc=', '.join)),
                left_index=True, right_index=True, how='left'
            )
        )

    data['albums'].release_date = pd.to_datetime(data['albums'].release_date, format='mixed').dt.strftime('%Y-%m-%d')

    # List of every genre associated to every track
    if not data['tracks'].empty and not data['artists'].empty and not data['albums'].empty and not data[
        'groups_genres'].empty and not data['genres'].empty \
            and not data['tracks_playlists'].empty and not data['playlists'].empty:
        df = data['tracks'].reset_index(names='track_id')
        genre_hierarchy = (
            pd.concat(
                [df.merge(data['teams']).merge(data['artists'], left_on='artist_id', right_index=True)[
                     ['track_id', 'genre_group_id']],
                 df.merge(data['albums'], left_on='album_id', right_index=True)[['track_id', 'genre_group_id']]]
            ).merge(data['groups_genres'])
            .merge(data['genres'], left_on='genre_id', right_index=True)
            .merge(data['tracks_playlists'])
            .merge(data['playlists'], left_on='playlist_id', right_index=True)
            .drop(columns=['track_id', 'genre_group_id', 'genre_id', 'playlist_id', 'date', 'description', 'public',
                           'n_tracks', 'genre'])
            .rename(columns={'name': 'genre_name'})
        )

    n_not_liked_tracks = 0
    if not data['tracks_playlists'].empty and not data['playlists'].empty and not teams_full.empty:
        df = data['tracks'].merge(data['tracks_playlists'][data['tracks_playlists'].playlist_id == 0], left_index=True,
                                  right_on='track_id',
                                  how='left')
        not_liked_tracks = (
            df[df.date.isnull()]
            .merge(data['playlists'], left_on='playlist_id', right_index=True)
            .groupby(['track_id', 'track_name', 'details', 'track_uri', 'team_id'], dropna=False)
            .agg(playlists=pd.NamedAgg(column='playlist_name', aggfunc=lambda var: ', '.join(var[var.notnull()])))
            .reset_index()
            .merge(teams_full, left_on='team_id', right_index=True)
            .drop(columns=['team_id', 'n_artist'], axis=1)
            .set_index('track_id')
        )
        n_not_liked_tracks = len(not_liked_tracks)

    n_not_categorized_tracks = 0
    if not tracks_in_playlists.empty:
        not_categorized_tracks = (
            tracks_in_playlists[tracks_in_playlists.occurrences == 0]
            .reset_index()
            .drop(columns=['index', 'occurrences', 'playlists', 'n_artist'])
            .rename(columns={'other_playlists': 'playlists'})
        )
        n_not_categorized_tracks = len(not_categorized_tracks)

    if not not_categorized_tracks.empty or not not_liked_tracks.empty:
        to_process = pd.concat([not_categorized_tracks, not_liked_tracks], ignore_index=True)

    if not data['playlists'].empty:
        playlist_groups = data['playlists'].groupby('group').agg(n_tracks=pd.NamedAgg(column='n_tracks', aggfunc='sum'),
                                                                 n_playlists=pd.NamedAgg(
                                                                     column='playlist_name', aggfunc='count'))

    # fig2.update_layout(showlegend=False, margin=dict(t=25, l=25, r=25, b=25), height=1500)
    # save_graph(fig2, '', 'playlist_release_dates_box_per_group', height=1500, overwrite=True)
    # fig2.show()

    # fig.update_layout(height=600, margin=dict(t=25, l=25, r=25, b=25))

    print("Identified", n_not_liked_tracks, "not liked tracks and",
          n_not_categorized_tracks, "tracks not added to any playlist")

    for key in data:
        data[key] = data[key].to_json(orient='split')
    datasets = {
        's': None,
        'user_id': None,
        'n_not_liked_tracks': n_not_liked_tracks,
        'raw_data': data,
        'teams_full': None if teams_full is None else teams_full.to_json(orient='split'),
        'tracks_full': None if tracks_full is None else tracks_full.to_json(orient='split'),
        'playlist_groups': None if playlist_groups is None else playlist_groups.to_json(orient='split'),
        'genre_hierarchy': None if genre_hierarchy is None else genre_hierarchy.to_json(orient='split'),
        'to_process': None if to_process is None else to_process.to_json(orient='split'),
    }
    return json.dumps(datasets)


# Selects the list of playlists to display
@app.callback(
    Output('playlist_choice', 'options'),
    Input('group_choice', 'value'),
    Input('data', 'data')
)
def sync_input(value, data):
    playlists = pd.read_json(StringIO(json.loads(data)['raw_data']['playlists']), orient='split')
    if value is None:
        print("\nGroups unselected")
    else:
        print("Group [", value, "] selected", sep='')
        playlists = playlists[playlists.group == value]
    return playlists.playlist_name.tolist()


# Generates the genres treemap
@app.callback(Output('load_genre_graph', 'figure'),
              Input('threshold', 'value'),
              Input('group_choice', 'value'),
              Input('playlist_choice', 'value'),
              Input('data', 'data'))
@app.callback(
    Output('genre_graph', 'figure'),
    Input('threshold', 'value'),
    Input('group_choice', 'value'),
    Input('playlist_choice', 'value'),
    Input('data', 'data'),
    prevent_initial_call=False
)
def sync_input(threshold, group_chosen, playlist_chosen, data):
    genre_hierarchy = pd.read_json(StringIO(json.loads(data)['genre_hierarchy']), orient='split')

    fig = go.Figure()

    if not genre_hierarchy.empty:
        if playlist_chosen is not None:
            genre_hierarchy = genre_hierarchy[genre_hierarchy.playlist_name == playlist_chosen]
        elif group_chosen is not None:
            genre_hierarchy = genre_hierarchy[genre_hierarchy.group == group_chosen]
            if group_chosen == 'others':
                genre_hierarchy = genre_hierarchy[genre_hierarchy.playlist_name != 'Liked Songs']

        n_proto_genres = genre_hierarchy.proto_genre_name.nunique()  # Number of proto genres
        n_playlist_genres = len(genre_hierarchy)

        print(genre_hierarchy.genre_name.nunique(), "genres identified among",
              n_proto_genres, "proto genres in the selected playlists")

        # Occurrence count per genre
        genre_hierarchy = (
            genre_hierarchy.groupby(['genre_name', 'proto_genre_name'])
            .agg(count=pd.NamedAgg(column='genre_name', aggfunc='count'))
            .reset_index(names=['genre', 'proto-genre'])
        )

        # Creation of a dataframe referencing the hierarchy of proto-genres and genres in an ensemble named 'total'
        genre_hierarchy = pd.concat([
            # List of proto-genres, each belonging to the ensemble 'total'
            pd.concat([
                genre_hierarchy.groupby('proto-genre')['count'].sum().reset_index().rename(
                    columns={'proto-genre': 'genre'}),
                pd.Series(['total'] * n_proto_genres, name='proto-genre')], axis=1),
            # List of genres, each belonging to a proto-genre
            genre_hierarchy,
            # Total
            pd.DataFrame({'genre': ['total'], 'count': [n_playlist_genres], 'proto-genre': [""]})
        ], ignore_index=True)
        # Identify the genres which have the same name as their proto-group and removes them from the dataset
        genre_hierarchy.drop(
            genre_hierarchy.loc[
                genre_hierarchy[
                    genre_hierarchy['genre'] == genre_hierarchy['proto-genre']].index,
                ['genre', 'count']].index, inplace=True)

        # Setting up a threshold to select the genres frequent enough to be shown
        if threshold > 0:
            genre_hierarchy = genre_hierarchy[genre_hierarchy['count'] >= threshold * n_playlist_genres / 100]

        # Removes the genres and proto-genres not frequent enough from the dataset

        print("Threshold set at", threshold, "%: ", len(genre_hierarchy), "genres and proto-genres after filtering")

        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            fig.add_trace(go.Treemap(
                branchvalues='total',
                labels=genre_hierarchy['genre'],
                parents=genre_hierarchy['proto-genre'],
                values=100 * genre_hierarchy['count'] / n_playlist_genres,
                hovertemplate='<b>%{label} </b> <br> Percentage: %{value:.1f}%<br><extra></extra>'
            ))

    return fig


# Generates the playlist treemap
@app.callback(Output('load_playlist_treemap', 'figure'),
              Input('group_choice', 'value'),
              Input('data', 'data'))
@app.callback(
    Output('playlist_treemap', 'figure'),
    Input('group_choice', 'value'),
    Input('data', 'data'),
    prevent_initial_call=False
)
def sync_input(group_chosen, data):
    datasets = json.loads(data)
    playlists = pd.read_json(StringIO(datasets['raw_data']['playlists']), orient='split')

    fig = go.Figure()

    if not playlists.empty:
        playlists = playlists[playlists.index != 0]

        if group_chosen is not None:
            playlists = playlists[playlists.group == group_chosen]

        n_tracks = playlists.n_tracks.sum() + datasets['n_not_liked_tracks']  # Number of proto genres
        n_playlists = len(playlists)
        n_groups = playlists.group.nunique()

        print(n_tracks, "tracks among", n_playlists, "playlists")

        # Creation of a dataframe referencing the hierarchy of proto-genres and genres in an ensemble named 'total'
        playlists = pd.concat([
            # List of proto-genres, each belonging to the ensemble 'total'
            pd.concat([
                playlists.groupby('group').agg(
                    n_tracks=pd.NamedAgg(column='n_tracks', aggfunc='sum')).reset_index().rename(
                    columns={'group': 'playlist_name'}),
                pd.Series(['total'] * n_groups, name='group')], axis=1),
            # List of genres, each belonging to a proto-genre
            playlists.drop(columns=['description', 'public', 'genre']),
            # Total
            pd.DataFrame({'playlist_name': ['total'], 'n_tracks': [n_tracks], 'group': [""]})
        ], ignore_index=True)
        # Identify the genres which have the same name as their proto-group and removes them from the dataset

        playlists.drop(playlists[playlists['playlist_name'] == playlists['group']].index, inplace=True)
        # Removes the genres and proto-genres not frequent enough from the dataset

        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            fig.add_trace(go.Treemap(
                branchvalues='total',
                labels=playlists['playlist_name'],
                parents=playlists['group'],
                values=100 * playlists['n_tracks'] / n_tracks,
                hovertemplate='<b>%{label} </b> <br> Percentage: %{value:.1f}%<br><extra></extra>'
            ))

    return fig


# Operates the data import button
@callback(
    Output('import_data_button', 'children'),
    Input('import_data', 'n_clicks'),
    State('df_to_reset', 'value'),
    prevent_initial_call=True
)
def update_output(n_clicks, to_reset):
    retrieve_data(to_reset)
    return "Data imported"


# Operates the 'original tracks' playlist generation button
@callback(
    Output('original_tracks_output', 'children'),
    Input('original_tracks', 'n_clicks'),
    State('data', 'data'),
    prevent_initial_call=True
)
def update_output(n_clicks, data):
    datasets = json.loads(data)
    playlists = pd.read_json(StringIO(datasets['raw_data']['playlists']), orient='split')
    tracks = pd.read_json(StringIO(datasets['raw_data']['tracks']), orient='split')

    if datasets['s'] is None:
        print("Connect to Spotify")
        datasets['s'], datasets['user_id'] = connect_to_Spotify()
        print("Connected to Spotify")

    to_process = identify_non_original_tracks(datasets['s'], generate_playlist=True)
    # to_process.to_pickle('data/original_tracks.pkl')

    playlist_name = 'original tracks'
    if not playlists.empty and playlist_name in playlists['playlist_name']:
        playlist_id = playlists[playlists['playlist_name'] == playlist_name].index[0]
    else:
        playlist = datasets['s'].playlist_create(
            datasets['user_id'], playlist_name, public=False,
            description='These songs are either not liked or not added to any playlist')
        playlist_id = playlist.id
        print("Playlist created")

    if not to_process.empty:
        _, failed_items, not_queried = sequence_queries(datasets['s'], to_process.uri.tolist(),
                                                        'track', query_type='add', complementary=playlist_id)
        if len(failed_items) > 0:
            print("Couldn't process ", end='')
            for item_id in failed_items:
                print("[", tracks.loc[item_id, 'track_name'], "] (", item_id, "),", sep='')

    return [
        "A 'original tracks' playlist with " + str(len(to_process)) + " songs as successfully been created",
        dash_table.DataTable(to_process.to_dict('records'), sort_action="native")
    ]


# Operates the playlist generation button
@app.callback(Output('load_to_categorize_output', 'children'),
              Input('to_categorize', 'n_clicks'),
              State('data', 'data'))
@callback(
    Output('to_categorize_output', 'children'),
    Input('to_categorize', 'n_clicks'),
    State('data', 'data'),
    prevent_initial_call=True
)
def update_output(n_clicks, data):
    datasets = json.loads(data)
    playlists = pd.read_json(StringIO(datasets['raw_data']['playlists']), orient='split')
    to_process = pd.read_json(StringIO(datasets['to_process']), orient='split')
    tracks = pd.read_json(StringIO(datasets['raw_data']['tracks']), orient='split')

    if datasets['s'] is None:
        print("Let's connect to Spotify")
        datasets['s'], datasets['user_id'] = connect_to_Spotify()
        print("Connected to Spotify")

    playlist_name = 'to categorize'
    if not playlists.empty and playlist_name in playlists['playlist_name']:
        playlist_id = playlists[playlists['playlist_name'] == playlist_name].index[0]
    else:
        playlist = datasets['s'].playlist_create(
            datasets['user_id'], playlist_name, public=False,
            description='These songs are either not liked or not added to any playlist')
        playlist_id = playlist.id
        print("Playlist created")

    if not to_process.empty:
        _, failed_items, not_queried = sequence_queries(datasets['s'], to_process.track_uri.tolist(),
                                                        'track', query_type='add', complementary=playlist_id)
        if len(failed_items) > 0:
            print("Couldn't process ", end='')
            for item_id in failed_items:
                print("[", tracks.loc[item_id, 'track_name'], "] (", item_id, "),", sep='')

    return "A 'to categorize' playlist with " + str(len(to_process)) + " songs as successfully been created"


# Generates the box plot
@app.callback(Output('load_box_playlists_graph', 'figure'),
              Input(component_id='stat-chosen', component_property='value'),
              Input('data', 'data'))
@callback(
    Output(component_id='playlists-graph', component_property='figure'),
    # Output('loading-icon', 'children'),
    Input(component_id='stat-chosen', component_property='value'),
    Input('data', 'data')
)
def update_graph(col_chosen, data):
    datasets = json.loads(data)
    playlist_groups = pd.read_json(StringIO(datasets['playlist_groups']), orient='split')
    tracks = pd.read_json(StringIO(datasets['raw_data']['tracks']), orient='split')
    albums = pd.read_json(StringIO(datasets['raw_data']['albums']), orient='split')
    playlists = pd.read_json(StringIO(datasets['raw_data']['playlists']), orient='split')
    tracks_playlists = pd.read_json(StringIO(datasets['raw_data']['tracks_playlists']), orient='split')
    n_playlists = len(playlists)

    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        if playlist_groups.empty:
            fig = go.Figure()
        else:
            fig = make_subplots(rows=len(playlist_groups), cols=1, vertical_spacing=0.07,
                                row_heights=[playlist_groups.iloc[i, 1] / n_playlists for i in
                                             range(len(playlist_groups))],
                                subplot_titles=[playlist_groups.index[i] for i in range(len(playlist_groups))],
                                shared_xaxes=True)

        if not playlists.empty and not tracks_playlists.empty and not albums.empty and not tracks.empty:
            albums.release_date = pd.to_datetime(albums.release_date)
            tracks.duration = pd.to_datetime(tracks.duration, unit='ms')  # pd.to_timedelta(tracks.duration, unit='ms')
            df = playlists.reindex(tracks_playlists.merge(tracks, left_on='track_id', right_index=True)
                                   .merge(albums, left_on='album_id', right_index=True)
                                   .groupby('playlist_id')
                                   [col_chosen].median().sort_values(ascending=False).index)
            for i in range(len(playlist_groups)):
                for playlist_id in df[df.group == playlist_groups.index[i]].index:
                    fig.add_trace(go.Box(x=tracks_playlists[tracks_playlists.playlist_id == playlist_id]
                                         .merge(tracks, left_on='track_id', right_index=True)
                                         .merge(albums, left_on='album_id', right_index=True)[col_chosen],
                                         name=df.loc[playlist_id, 'playlist_name'],
                                         boxpoints=False
                                         ),
                                  row=i + 1, col=1)
        fig.update_xaxes(showticklabels=True)
        fig.update_layout(showlegend=False, margin=dict(t=25, l=25, r=25, b=25))
    return fig


# Generates the playlist table
@app.callback(Output('load_playlist_table', 'figure'),
              Input('group_choice', 'value'),
              Input('playlist_choice', 'value'),
              Input('data', 'data'))
@callback(
    Output('playlist_table', 'data'),
    Input('group_choice', 'value'),
    Input('playlist_choice', 'value'),
    Input('data', 'data')
)
def update_table(group_chosen, playlist_chosen, data):
    datasets = json.loads(data)
    tracks_full = pd.read_json(StringIO(datasets['tracks_full']), orient='split')
    teams_full = pd.read_json(StringIO(datasets['teams_full']), orient='split')
    albums = pd.read_json(StringIO(datasets['raw_data']['albums']), orient='split')

    if not tracks_full.empty:
        if playlist_chosen is not None:
            tracks_full = tracks_full[tracks_full.playlist_name == playlist_chosen]
        elif group_chosen is not None:
            tracks_full = tracks_full[tracks_full.group == group_chosen]

        tracks_full = (tracks_full.merge(teams_full, left_on='team_id', right_index=True)
                       .drop(columns=['team_id', 'playlist_name', 'group'])
                       .drop_duplicates()
                       .merge(albums, left_on='album_id', right_index=True)
                       .drop(columns=['track_uri', 'album_id', 'album_team_id', 'album_uri'])
                       )
        tracks_full.duration = round(tracks_full.duration / 60000, 2)
    return tracks_full.to_dict('records')


# Generates the to categorize table
@app.callback(Output('load_to_categorize_table', 'figure'),
              Input('data', 'data'))
@callback(
    Output('to_categorize_table', 'data'),
    Input('data', 'data')
)
def update_table(data):
    to_process = pd.read_json(StringIO(json.loads(data)['to_process']), orient='split')
    if not to_process.empty:
        to_process.drop(columns='track_uri', inplace=True)
    return to_process.to_dict('records')


if __name__ == '__main__':
    app.run(debug=True)
