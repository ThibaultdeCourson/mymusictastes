# MyMusicTastes

A tool to analyse the music tastes of the user.
Handy for creating playlists to suit all your moods.

## The Manage data tab
Here you can import your data from Spotify and generate various playlists
![Manage data tab](img/manage_tab.png)

## The playlist analysis tab
Here you can compare the size of your playlists and the genres of the tracks they contain.
![Playlist analysis tab](img/playlist_tab.png)
On this picture the Rock playlists are selected

## The explore data tab
On this tab you can compare the spread of the release date, the duration or the popularity of your songs
depending on their playlist
![Explore data tab p1](img/explore_tab_1.png)
![Explore data tab p2](img/explore_tab_2.png)
