import tekore as tk
import pickle
from os.path import isfile
import time
from math import ceil
from pydantic.error_wrappers import ValidationError


def sequence_queries(s, data, data_type, query_type='get', complementary=None):
    start = time.time()

    n_data = len(data)
    chunk_size = 100
    if data_type == 'album':
        chunk_size = 20
    elif data_type == 'artist':
        chunk_size = 50
    print(n_data, " ", data_type, "s to process \n[", " " * ceil(n_data / chunk_size), "]\n[", sep='', end='')

    output = [[], []]
    failed_items = []
    not_queried = []
    too_many_requests = False

    i = 0
    while i < len(data):
        ids = data[i:i + chunk_size]

        try:
            if query_type == 'add':
                s.playlist_add(complementary, ids)
            else:

                for item in s.albums(ids) if data_type == "album" else s.artists(ids):
                    output[0].append(item.id)
                    if data_type == "album":
                        output[1].append([item, item.popularity])
                    else:
                        output[1].append([item.followers.total, item, item.popularity])

            i += chunk_size
            print("-", sep='', end='')

        except tk.BadRequest as e:
            failed_item = str(e)[str(e).find('Invalid track uri:') + 19:-1]
            data.remove(failed_item)
            failed_items.append(failed_item)

        except (tk.TooManyRequests, tk.InternalServerError) as e:
            print(" ", sep='', end='')
            i += chunk_size
            too_many_requests = True
            not_queried += ids

        except ValidationError as e:
            print("!", sep='', end='')

            if query_type != 'add':
                items = []

            for item_id in ids:
                try:
                    if query_type == 'add':
                        s.playlist_add(complementary, item_id)
                    else:
                        item = s.album(item_id) if data_type == "album" else s.artist(item_id)
                        output[0].append(item.id)
                        if data_type == "album":
                            output[1].append([item, item.popularity])
                        else:
                            output[1].append([item.followers.total, item, item.popularity])

                except (tk.BadRequest, ValidationError):
                    failed_items.append(item_id)
                except tk.TooManyRequests:
                    too_many_requests = True
                    not_queried.append(item_id)
            i += chunk_size

    print("] ", round(time.time() - start), "s\n", sep='', end='')
    if too_many_requests:
        print("Too many requests to the Spotify API")

    return output, failed_items, not_queried


def connect_to_Spotify():
    # Import authentification data saved locally
    filename = "authentification_data.txt"
    x= []
    if isfile(filename):
        # Open the file and read its content.
        with open(filename) as f:
            x = [x for x in f.read().splitlines() if x]
            if len(x) != 3:
                print("ERROR: authentification data file not correctly formatted.\n" +
                      "The file must contain three line, containing respectively " +
                      "your user id, client id and client secret (in this order)")

    if len(x) == 3:
        user_id, client_id, client_secret = x
    else:
        user_id = input("Enter your user id:")
        client_id = input("Enter your client id:")
        client_secret = input("Enter your client secret:")

    conf = (client_id, client_secret, 'https://example.com/callback')
    s = tk.Spotify(tk.prompt_for_user_token(*conf, scope=tk.scope.every))
    print("Successfully logged in Spotify\n")
    return s, user_id


def save_graph(fig, path, name, height=850, show=False, overwrite=False, transparency=False):
    """ Save a Plotly graph locally

    @param fig: Plotly Figure
    @param path: string
        Path to the saving directory
    @param name: String
        Filename for the graph
    @param height: Int
        Graph height
    @param show: boolean
        Display the graph in the user's interface
    @param overwrite: boolean
        Overwrite local file
    @param transparency: boolean
        Use a transparent background

    @author Thibault de Courson, Epsiline, 2022
    """

    fig.update_layout(template='plotly_dark', height=height, hoverlabel=dict(namelength=-1))
    if transparency:
        fig.update_layout(paper_bgcolor='rgba(0,0,0,0)', plot_bgcolor='rgba(0,0,0,0)')
        name += '_transp'

    # data_size = sum([np.sum(~pd.isna(i.y)) if i.y is not None and len(i.y) > 10 else 0 for i in fig.data])
    data_size = 1

    if overwrite or not isfile('output/' + path + 'img/' + name + '.png'):
        if data_size < 22 * (10 ** 5):
            fig.write_image('output/' + path + 'img/' + name + '.png', width=1920)
        else:
            print("ERROR: data to heavy to save:", data_size)
    if overwrite or not isfile('output/' + path + name + '.html'):
        if data_size < 8 * (10 ** 5):
            fig.write_html('output/' + path + name + '.html')
        else:
            print("ERROR: data to heavy to save as html file:", data_size)
    if show and data_size < 8 * (10 ** 5):
        fig.show()
