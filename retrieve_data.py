import time
from os.path import isfile
import pandas as pd
from common import connect_to_Spotify, sequence_queries
from datetime import datetime
import tekore as tk
import string

df_columns = {'artists': ['artist_name', 'followers', 'genre_group_id', 'popularity', 'artist_uri'],
              'teams': ['team_id', 'artist_id'],
              'tracks': ['track_name', 'details', 'album_id', 'team_id', 'duration', 'explicit', 'track_uri',
                         'playable'],
              'playlists': ['playlist_name', 'description', 'public'],
              'tracks_playlists': ['playlist_id', 'track_id', 'date'],
              'albums': ['album_name', 'album_team_id', 'album_type', 'album_size', 'genre_group_id', 'popularity',
                         'release_date', 'album_uri'],
              'groups_genres': ['genre_group_id', 'genre_id'],
              'genres': ['name']}

for variable in ('artists', 'teams', 'tracks', 'playlists', 'tracks_playlists', 'albums', 'groups_genres', 'genres'):
    filename = 'data/' + variable + '.pkl'
    if isfile(filename):
        vars()[variable] = pd.read_pickle(filename)
    else:
        vars()[variable] = pd.DataFrame(columns=df_columns[variable])
n_genres = 0
n_teams = 0
n_genre_groups = 0
change = None
data_list = {'album': [], 'artist': []}
market = 'FR'


def iterate_through_genres(source):
    """
    Search for genres associated to the source and saves the association in the database

    :param source: tekore.model.SimpleArtist or tekore.model.SimpleAlbum
    :return: genre group id
    """

    global n_genres
    global groups_genres
    global n_genre_groups
    global genres

    if len(source.genres) > 0:
        # List the genres associated to the source
        genre_list = set()
        for genre in source.genres:
            # Checks if the genre has already been saved

            if any(genres['name'].isin([genre])):
                genre_list.add(genres[genres['name'] == genre].index[0])
            else:
                # Saves the new genre
                genre_list.add(n_genres)
                n_genres += 1
                genres = pd.concat([genres, pd.DataFrame({'name': [genre]})], ignore_index=True)

        # Checks if the genre group has already been saved
        for genre_group_id, this_genre_group in groups_genres.groupby('genre_group_id'):
            # Search for a group containing the same genres

            if genre_list == set(this_genre_group['genre_id']):
                return genre_group_id

        # Saves the group
        groups_genres = pd.concat(
            [groups_genres,
             pd.DataFrame({'genre_group_id': [n_genre_groups] * len(genre_list), 'genre_id': list(genre_list)})],
            ignore_index=True)
        n_genre_groups += 1
        return n_genre_groups - 1
    else:
        return None


def iterate_through_playlist(var, playlist_id):
    """
    Adds the playlist and its content (tracks, artists, albums) to the database

    :param var: tekore.model.PlaylistTrackPaging or tekore.model.SavedTrackPaging
    :param playlist_id: id of the playlist to save
    :return: None
    """

    global artists
    global teams
    global tracks
    global tracks_playlists
    global albums
    global n_teams
    global n_genres
    global n_genre_groups
    global data_list
    global change

    # Iterates through tracks
    for item in var:

        # Saves playlist-track association

        if item.track is None:
            print("\nStrange track:\n", item)
        if (tracks_playlists == (playlist_id, item.track.id, str(item.added_at)[:19])).all(axis=1).sum() == 0:
            df2 = pd.DataFrame({'playlist_id': [playlist_id], 'track_id': [item.track.id], 'date': [item.added_at]})
            tracks_playlists = df2 if tracks_playlists.empty else pd.concat([tracks_playlists, df2],
                                                                            ignore_index=True)
            change['playlist_content'] = True

        # Checks if the track has already been saved
        if item.track.id not in tracks.index:
            change['tracks'] = True

            # Generates the artists list (team)
            team = set()
            for artist in item.track.artists:
                team.add(artist.id)

                # Checks if the artist has already been saved
                if artist.id not in artists:
                    change['artist'] = True

                    # Saves the artist
                    data_list['artist'].append(artist.id)

                    # Create the artist
                    artists.loc[artist.id] = (artist.name, None, None, None, artist.uri)

            # Checks if the team has already been saved
            this_team_id = None
            for i, this_team in teams.groupby('team_id'):
                # Search for a team containing the same artists

                if team == set(this_team['artist_id']):
                    this_team_id = i
                    break

            if this_team_id is None:
                change['team'] = True

                # Saves the team
                teams = pd.concat(
                    [teams, pd.DataFrame({'team_id': [n_teams] * len(team), 'artist_id': list(team)})],
                    ignore_index=True)
                this_team_id = n_teams
                n_teams += 1

            # Checks if the album has already been saved
            if item.track.album.id not in albums.index:
                change['album'] = True
                data_list['album'].append(item.track.album.id)
                albums.loc[item.track.album.id] = (item.track.album.name, this_team_id,
                                                   str(item.track.album.album_type),
                                                   item.track.album.total_tracks, None,
                                                   None, item.track.album.release_date,
                                                   item.track.album.uri)

            # Saves the track
            tracks.loc[item.track.id] = (item.track.name,
                                         None,
                                         item.track.album.id,
                                         this_team_id,
                                         item.track.duration_ms,
                                         item.track.explicit,
                                         item.track.uri,
                                         item.track.is_playable)


def search_original_track(s, track_name, artist_names, release_date=None, album_type='collection', verbose=False):
    if type(artist_names) is not set:
        artist_names = {artist_names}
    if release_date is None:
        release_date = datetime.now()
    track_name = track_name.lower().replace("'", ' ').translate(str.maketrans('', '', string.punctuation))
    # if type(release_date) is str:
    #     release_date = pd.to_datetime(release_date)
    data = None
    artist_names_str = ' '.join(artist_names)
    if track_name is None or artist_names is None or verbose:
        print("\n[", track_name, "] by [", artist_names_str, "]: current release date: [", release_date.date(), "]:",
              sep='')
    try:
        search_r, = s.search(track_name + " " + artist_names_str, market=market, include_external='audio', limit=50)

        for track_found in search_r.items:
            release_date_found = pd.to_datetime(track_found.album.release_date)
            name_found = track_found.name.lower().replace("'", ' ').translate(str.maketrans('', '', string.punctuation))
            album_type_found = track_found.album.album_type
            name_cond = name_found.find(track_name) != -1
            artists = set([artist.name.lower().replace("'", ' ').translate(str.maketrans('', '', string.punctuation))
                           for artist in track_found.artists])
            artist_cond = artists == artist_names
            date_cond = release_date_found.year < release_date.year
            if verbose and date_cond:
                # [", release_date_found.date(), "] (", date_cond,"),
                print(date_cond and name_cond and artist_cond, ": [", name_found, "]: ", name_cond, ", [",
                      ', '.join(artists), "]: ", artist_cond, ", [",
                      album_type_found, "]", sep='')
            if release_date_found.year <= release_date.year and name_cond and artist_cond:
                if date_cond or (album_type == 'compilation' and album_type_found != 'compilation'):
                    release_date = release_date_found
                    data = [name_found, '', '', track_found.album.name, album_type_found, release_date_found,
                            track_found.uri]
    except tk.BadGateway as e:
        print("Fail:", e)
    return data


def identify_non_original_tracks(s, generate_playlist=False):
    if generate_playlist:
        playlist = pd.DataFrame(columns=['type', 'name', 'details', 'artists', 'album', 'album type', 'release date',
                                         'uri'])
    else:
        playlist = None

    tracks_full = tracks.merge(albums, left_on='album_id', right_index=True)
    keywords = 'remaster|essential|greatest|best|meilleur|tubes|hits|numeros un|succès|version|edition' + \
               '|anniversaire|anthology|collection|singles|anniversary|mix|playlist|throwback'
    to_process = (
        tracks_full[(tracks_full.details.notnull() & tracks_full.details.str.lower().str.contains(keywords))
                    | (tracks_full.album_type == 'compilation')
                    | tracks_full.album_name.str.lower().str.contains(keywords)]
        .reset_index(names='track_id')
        .merge(teams)
        .merge(artists, left_on='artist_id', right_index=True)
    )
    to_process.artist_name = to_process.artist_name.str.lower().str.replace("'", ' ').str.translate(
        str.maketrans('', '', string.punctuation))
    to_process = (
        to_process.groupby(['track_id', 'release_date', 'track_name', 'details', 'track_uri', 'album_id', 'album_type',
                            'album_name'], dropna=False)
        .agg(artist_names=pd.NamedAgg(column='artist_name', aggfunc=set))
        .reset_index()
    )

    to_process.release_date = pd.to_datetime(to_process.release_date, format='mixed')

    modified = 0
    print(len(to_process), "tracks to correct among", len(tracks_full))
    start = time.time()
    for _, liked_track in to_process.sort_values('release_date').iterrows():
        data = search_original_track(s, liked_track['track_name'], liked_track['artist_names'],
                                     liked_track['release_date'], liked_track['album_type'])  # , verbose=True

        if data is None:
            print("No release date prior to [", liked_track['release_date'].date(), "] found for ",
                  liked_track['track_name'], " by ", ', '.join(liked_track['artist_names']), sep='')
        else:
            modified += 1
            albums.loc[liked_track['album_id'], ['album_name', 'album_type', 'release_date', 'album_uri']] = [
                data[3], data[4], data[5].date(), data[6]
            ]

            if generate_playlist:
                playlist.loc[modified * 2 - 2] = [
                    'liked', liked_track['track_name'], liked_track['details'], liked_track['artist_names'],
                    liked_track['album_name'], liked_track['album_type'], liked_track['release_date'],
                    liked_track['track_uri']]
                playlist.loc[modified * 2 - 1] = ['original'] + data

            print("[", liked_track['release_date'].date(), "] replaced by [", data[5].date(), "] for ",
                  liked_track['track_name'], " by ", ', '.join(liked_track['artist_names']),
                  " (https://open.spotify.com/track/", data[6][data[6].find('track:') + 6:], ")", sep='')
        # else: print("original date (", liked_track['release_date'].date(), ") kept for [", sep='', end='')

    print(modified, "/", len(to_process), " tracks corrected (", round(time.time() - start), "s)", sep='')
    return playlist


def retrieve_data(to_reset=None, market='FR'):
    global playlists
    global genres
    global change

    change = {'playlists': False, 'playlist_content': False, 'tracks': False, 'artist': False, 'team': False,
              'album': False, 'genres': False}

    if to_reset is not None:
        if 'artists' in to_reset:
            artists.drop(artists.index, inplace=True)
            teams.drop(teams.index, inplace=True)
        if 'albums' in to_reset:
            albums.drop(albums.index, inplace=True)
            to_reset += ['tracks']
        if 'tracks' in to_reset:
            tracks.drop(tracks.index, inplace=True)
        if 'playlists' in to_reset:
            playlists = pd.DataFrame(columns=df_columns['playlists'])
            tracks_playlists.drop(tracks_playlists.index, inplace=True)
        if 'genres':
            genres = pd.DataFrame(columns=df_columns['genres'])
            groups_genres.drop(groups_genres.index, inplace=True)

    s, user_id = connect_to_Spotify()

    # Import the name of the playlists to exclude
    playlists_to_exclude = ['to categorize', 'original tracks']
    filename = "playlists_to_exclude.txt"
    if isfile(filename):
        # Open the file and read its content.
        with open(filename) as f:
            playlists_to_exclude += [x for x in f.read().splitlines() if x]

    # Import liked songs
    start = time.time()
    print("Playlist [Liked songs] selected", end='')
    if 0 not in playlists.index:
        playlists.loc[0] = ("Liked songs", "", False)
    x = s.saved_tracks(market=market)
    iterate_through_playlist(s.all_items(x), 0)
    print(" (", x.total, " tracks)\n", sep='')

    # Import user playlists
    for playlist in s.all_items(s.followed_playlists()):

        # Eliminates the playlists not created by the user
        if playlist.owner.id == user_id and playlist.name not in playlists_to_exclude:
            print("Playlist [", playlist.name, "] selected", sep='', end='')

            # Saves the playlist
            if playlist.id not in playlists.index:
                change['playlists'] = True
                playlists.loc[playlist.id] = (playlist.name, playlist.description, playlist.public)

            # Saves the content of the playlist
            x = s.playlist_items(playlist.id, market=market)
            iterate_through_playlist(s.all_items(x), playlist.id)
            print(" (", x.total, " tracks)", sep='')
        else:
            print("Playlist [", playlist.name, "] ignored", sep='')
    print("Import duration: ", round(time.time() - start), "s\n", sep='')

    # Add information to the playlists table
    # Separate playlists by categories
    if change['playlists']:
        playlists['group'] = 'others'
        playlists.loc[playlists[playlists.playlist_name.str.lower().str.contains('pop')].index, 'group'] = 'pop'
        playlists.loc[
            playlists[playlists.playlist_name.str.lower().str.contains('rock|alternative')].index, 'group'] = 'rock'
        playlists.loc[playlists[
            playlists.playlist_name.str.lower().str.contains('electro|transe|dance|house')].index, 'group'] = 'electro'
        playlists.loc[playlists[playlists.playlist_name.str.lower().str.contains('rap|hip-hop')].index, 'group'] = \
            'hip-hop'
        playlists.loc[playlists[playlists.playlist_name.str.contains('BOs|Génériques')].index, 'group'] = 'BOs'

        # Generates the median release date of the playlists
        df = (
            playlists.merge(tracks_playlists, left_index=True, right_on='playlist_id')
            .merge(tracks, left_on='track_id', right_index=True)
            .merge(albums, left_on='album_id', right_index=True)
        )

        # Check if album release dates have a readable format
        incorrect_dates = df[~df.release_date.str.len().isin([4, 7, 10])]
        if len(incorrect_dates) > 0:
            print('Anormal dates:\n', incorrect_dates)

        df['release_date'] = pd.to_datetime(df.release_date, format='mixed')

        # Generates the track number of the playlists
        df = (
            df.groupby('playlist_id')
            .agg(n_tracks=pd.NamedAgg(column='track_id', aggfunc='count'))
        )

        # Adds the new information
        playlists = playlists.merge(df, left_index=True, right_index=True).sort_values(['group'])

    # Get complementary information on artists and albums
    for data_type in ('artist', 'album'):
        if change[data_type]:
            output, failed_items, not_queried = sequence_queries(s, data_list[data_type], data_type)

            pos = 0 if data_type == 'album' else 1
            for i in range(len(output[1])):
                output[1][i][pos] = iterate_through_genres(output[1][i][pos])
            if data_type == 'album':
                albums.loc[output[0], ['genre_group_id', 'popularity']] = output[1]
            else:
                artists.loc[output[0], ['followers', 'genre_group_id', 'popularity']] = output[1]

            for item_id in failed_items:
                if data_type == 'album':
                    name = albums.loc[item_id, 'name']
                elif data_type == 'artist':
                    name = artists.loc[item_id, 'name']
                else:
                    name = tracks.loc[item_id, 'name']

                print("Couldn't process [", name, "] (", item_id, ")", sep='')

    # Adds the genre information
    # Import the list of not-genre playlists
    not_genre_playlists = ['Liked songs']
    filename = 'not_genre_playlists.txt'
    if isfile(filename):
        # Open the file and read its content.
        with open(filename) as f:
            not_genre_playlists += [x for x in f.read().splitlines() if x]
    playlists['genre'] = True
    playlists.loc[playlists[playlists.playlist_name.isin(not_genre_playlists)].index, ['genre']] = False

    if change['tracks']:
        # Reformatting track names (removing remaster or remix mentions)
        with_brackets = tracks[tracks['track_name'].str.contains(' \(')].index
        cut_name = tracks.loc[with_brackets, 'track_name'].str.split(' \(', expand=True, n=1)
        cut_name.columns = ['track_name', 'details']
        tracks.loc[with_brackets, ['track_name', 'details']] = cut_name

        with_hyphen = tracks[tracks['track_name'].str.contains(' - ')].index
        cut_names = tracks.loc[with_hyphen, 'track_name'].str.split(' - ', expand=True, n=1)
        tracks.loc[with_hyphen, 'track_name'] = cut_names[0]
        tracks.loc[with_hyphen, 'details'] = \
            cut_names[1] + ' ' + tracks.loc[with_hyphen, 'details'].astype(str).replace('None', '')
        tracks.details = tracks.details.str.replace(')', '').str.replace('(', '')

    if change['album']:
        print("\nFetching original release date of remastered songs")
        identify_non_original_tracks(s)

    if change['artist'] or change['album'] or change['genres']:
        modifications = ((('hip hop', 'hip-hop'), ('hip pop', 'hip-hop'), ('alt z', 'alt-z'), ('ye ye', 'ye-ye'),
                          ('beatlesque', 'beatle'), ('hands up', 'hands-up'), ('british invasion', 'british-invasion'),
                          (' international', ''), ('mellow gold', 'mellow-gold')),
                         (('electropop', 'electro pop'), ('synthpop', 'synth pop'), ('metalcore', 'metal hardcore'),
                          ('britpop', 'pop'), ('dance-punk', 'dance punk'), ('post-grunge', 'rock'),
                          ('merseybeat', 'beat'),
                          ('austropop', 'pop'), ('rock-and-roll', 'rock'), ('rockabilly', 'rock'),
                          ('proto-hyperpop', 'pop'),
                          ('scandipop', 'pop'), ('europop', 'pop'), ('protopunk', 'punk'), ('hyperpop', 'pop'),
                          ('electrofox', 'electro'), ('indietronica', 'electronica'), ('eurodance', 'dance'),
                          ('sophisti-pop', 'pop'), ('trancecore', 'trance hardcore'), ('aussietronica', 'electronica'),
                          ('chillwave', 'chill wave'), ('j-rock', 'rock'), ('g-house', 'ghetto house'),
                          ('grungegaze', 'rock shoegaze'), ('afropop', 'pop'), ('chillsynth', 'chill synth'),
                          ('synthwave', 'synth wave'), ('proto-metal', 'metal'), ('filthstep', 'step'),
                          ('bossbeat', 'beat'),
                          ('post-hardcore', 'hardcore'), ('hardstyle', 'hard'), ('neo-classical', 'classical'),
                          ('cyberpunk', 'punk'), ('darksynth', 'synth'), ('dancefloor', 'dance'), ('nederpop', 'pop'),
                          ('k-rap', 'rap'), ('livetronica', 'electronica'), ('jazztronica', 'jazz electronica'),
                          ('eurobeat', 'beat'), ('reggaeton', 'reggae'), ('k-pop', 'pop'), ('breakbeat', 'beat'),
                          ('post-punk', 'punk'), ("punk 'n' roll", 'punk rock'), ('a cappella', 'acappella'),
                          ('sped up', 'sped-up'), ('beatle', 'rock '), ('grunge', 'rock'),
                          ('edm', 'electronic dance music'),
                          ('electronica', 'electronic'), ('wave', 'electronic'), ('house', 'electronic'),
                          ('metal', 'rock'),
                          ('hands-up', 'electronic'), ('punk', 'rock'), ('complextro', 'electronic'),
                          ('big room', 'electronic'), ('pixel', 'electronic'), ('melbourne bounce', 'electronic'),
                          ('disco', 'dance')
                          ))

        for modification in modifications[0]:
            genres['name'] = genres['name'].str.replace(modification[0], modification[1])

        proto_genre_asso = genres['name']
        for modification in modifications[1]:
            proto_genre_asso = proto_genre_asso.str.replace(modification[0], modification[1])
        for modification in (
                'canadian', 'german', 'uk', 'swedish', 'british', 'french', 'quebecois', 'italiano', 'italian',
                'francaise', 'australian', 'francais'):
            proto_genre_asso = proto_genre_asso.str.replace(modification, '')
        proto_genre_asso = proto_genre_asso.str.strip().str.split(' ')
        proto_genre_asso.index.name = 'genre_id'
        proto_genre_asso.name = 'proto_genre_names'

        list_genres = pd.concat(
            [tracks.merge(teams).merge(artists, left_on='artist_id', right_index=True)[['genre_group_id']],
             tracks.merge(albums, left_on='album_id', right_index=True
                          )[['genre_group_id']]]).merge(groups_genres)

        proto_genre_asso = proto_genre_asso.explode().replace('electro', 'electronic')
        proto_genre_asso.name = 'proto_genre_name'

        proto_genre_asso.drop(proto_genre_asso[proto_genre_asso.str.len() == 0].index, inplace=True)

        # Number of occurrences for each proto-genre
        proto_genres = list_genres.merge(proto_genre_asso, left_on='genre_group_id', right_index=True
                                         ).proto_genre_name.value_counts()
        proto_genres.name = 'count'

        a = proto_genre_asso.reset_index().merge(proto_genres, left_on='proto_genre_name', right_index=True
                                                 ).groupby('genre_id')[['proto_genre_name', 'count']].apply(
            lambda var: var.loc[var['count'].idxmax(), 'proto_genre_name'])
        a.name = 'proto_genre_name'

        genres = genres.merge(a, left_index=True, right_on='genre_id')
        genres.index.name = 'genre_id'

    # Saves locally the data retrieved
    if change['artist']:
        artists.to_pickle('data/artists.pkl')
    if change['team']:
        teams.to_pickle('data/teams.pkl')
    if change['tracks']:
        tracks.to_pickle('data/tracks.pkl')
    if change['playlists']:
        playlists.to_pickle('data/playlists.pkl')
    if change['playlist_content']:
        tracks_playlists.to_pickle('data/tracks_playlists.pkl')
    if change['album']:
        albums.to_pickle('data/albums.pkl')
    if change['album'] or change['artist'] or change['genres']:
        genres.to_pickle('data/genres.pkl')
        groups_genres.to_pickle('data/groups_genres.pkl')
    if True in change.values():
        print("New data saved locally for", ', '.join([item for item in change if change[item]]))
    else:
        print("Not changes needed")


if __name__ == '__main__':
    # test1.py executed as script
    # do something
    retrieve_data()
